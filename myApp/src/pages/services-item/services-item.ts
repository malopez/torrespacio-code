import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-services-item',
  templateUrl: 'services-item.html'
})
export class ServicesItem {

  constructor(public navCtrl: NavController) {

  }

}

